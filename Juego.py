class Juego:    
    
    def __init__(self,miTablero, jugador1, jugador2):
        self.miTablero=miTablero
        self.jugador1=jugador1
        self.jugador2=jugador2
        
    def mensaje_bienvenida(self):
        print("TRIQUI CON PYTHON")
        print("Jugará contra el computador")
        print("=================================")
        print("Las posiciones del tablero son (fila,columna):")
        print("0,0 0,1 0,2")
        print("1,0 1,1 1,2")
        print("2,0 2,1 2,2")
        print("=================================")
        print("Empieza el juego")
        print("=================================")
    
    def seleccionar_fichas(self):
        tipo_seleccion=int(input("Ingrese 1 si desea seleccionar manualmente la ficha:"))   
        
        if tipo_seleccion==1:
            self.jugador1.seleccionar_ficha("humano")            
        else:
            self.jugador1.seleccionar_ficha("maquina")
        
        if self.jugador1.ficha.simbolo=="X":
            print("=============================")
            print("Vas a jugar con X")
            print("=============================")
            self.jugador2.ficha.simbolo="O"
        else:
            self.jugador2.ficha.simbolo="X"
        
        return True
        
    def jugar_triqui(self):
        
        self.mensaje_bienvenida()
        
        #Seleccionar fichas
        self.seleccionar_fichas()
                
        #Asignar turnos
        self.jugador1.turno=True
        self.jugador2.turno=False
        
        #Mostrar tablero
        self.miTablero.mostrar_tablero()
        
        #Realizar máximo 9 jugadas
        for i in range(0,9):
            if self.jugador1.turno:
                self.jugador1.realizar_jugada(self.miTablero)
                self.jugador1.turno=False
                self.jugador2.turno=True
            else:
                self.jugador2.realizar_jugada(self.miTablero)
                self.jugador1.turno=True
                self.jugador2.turno=False
                print("=====================================")
                print("El computador realizo su jugada")
                print("=====================================")
            
            #Mostrar tablero
            self.miTablero.mostrar_tablero()
            
            #Verificar si hay triqui
            if self.miTablero.verificar_triqui():
                print("Triqui!")
                break