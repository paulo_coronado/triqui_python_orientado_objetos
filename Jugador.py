import random
from Ficha import Ficha


class Jugador:
    
    def __init__(self, tipo):
        self.tipo=tipo
        self.ficha=Ficha()
        self.turno=False
    
    def realizar_jugada(self, tablero):
        
        if self.tipo=="maquina":
            self.jugada_maquina(tablero)
        else:
            self.jugada_humano(tablero)

    def jugada_maquina(self, tablero):
        while True:
            fila=random.randint(0,2)
            columna=random.randint(0,2)
            if tablero.verificar_jugada(fila, columna):
                tablero.colocar_ficha(fila, columna, self.ficha.simbolo)
                break
            
    def jugada_humano(self, tablero):
        #Realizar una jugada
        fila=int(input("Ingrese la fila: "))
        columna=int(input("Ingrese la columna: "))
        
        #Verificar si la jugada es valida
        if tablero.verificar_jugada(fila, columna):
            tablero.colocar_ficha(fila, columna, self.ficha.simbolo)
        else:
            print("Jugada invalida, por favor intente de nuevo")
            self.realizar_jugada(tablero)
                
            
    def seleccionar_ficha(self, tipo):        
        simbolos=["X","O"]
        if tipo=="maquina":
            self.ficha.simbolo=random.choice(simbolos)
        else:
            self.ficha.simbolo=input("Ingrese el simbolo de la ficha (X o O): ")
            if self.ficha.simbolo not in simbolos:
                print("Simbolo invalido, por favor intente de nuevo")
                self.seleccionar_ficha(tipo)