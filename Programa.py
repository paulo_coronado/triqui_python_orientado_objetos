from Tablero import Tablero
from Jugador import Jugador
from Juego import *


#Crear los objetos
miTablero=Tablero()
jugador1=Jugador("humano")
jugador2=Jugador("maquina")

#Inyectar las dependencias
miJuego=Juego(miTablero, jugador1, jugador2)

#Jugar triqui
miJuego.jugar_triqui()

