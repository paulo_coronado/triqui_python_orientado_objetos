# TRIQUI EN PYTHON
## Propósito
Proyecto de juego de triqui para explorar los conceptos básicos del paradigma orientado a objetos con leguaje de programación Python. 

## Nivel de Dificultad (0-10)
3

## Conceptos abordados

### Pensamiento computacional
* Descomposición
* Pensamiento algorítmico
* Abstracción.

### Paradigma de programación orientado a objetos
* Clase
  -  Atributos
  -  Métodos
* objetos
* Llamada a métodos
* Cohesión
* Inyección de dependencias
### Python
* class
  - def
  - \_\_init\_\_
  - self
* List comprehension
* List of lists
* random
* string.format()
* for
  - break
* if
* range()
* random.choice()
* random.randint()
* Cadenas de texto en múltiples línes (multi-line strings)
* input()
* int()
* print()
* == (igual a)
* != (diferente a)

## Nota

Recurso Educativo Abierto para el curso de programación de computadores para el desarrollo del pensamiento computacional en estudiantes de ingeniería. Este código fuente hace parte de una sesión de live coding que se encuentra en el canal de youtube de Paulo César Coronado (paulocoronado@udistrital.edu.co)







