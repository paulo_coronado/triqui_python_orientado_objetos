class Tablero:    
    
    def __init__(self):
        #Crear una lista de listas de 3x3 con " " en cada posicion
        self._cuadricula=[[" " for _ in range(0,3)] for _ in range(0,3)]
        
    def verificar_triqui(self):
        #Verificar si hay triqui en el tablero
        
        #Verificar si hay triqui en las filas
        for fila in range(0,3):
            if self._cuadricula[fila][0]==self._cuadricula[fila][1]==self._cuadricula[fila][2]!=" ":
                return True
            
        #Verificar si hay triqui en las columnas        
        for columna in range(0,3):
            if self._cuadricula[0][columna]==self._cuadricula[1][columna]==self._cuadricula[2][columna] and self._cuadricula[0][columna]!=" ":
                return True
        
        #Verificar si hay triqui en las diagonales
        if self._cuadricula[0][0]==self._cuadricula[1][1]==self._cuadricula[2][2] and self._cuadricula[0][0]!=" ":
            return True
        if self._cuadricula[0][2]==self._cuadricula[1][1]==self._cuadricula[2][0] and self._cuadricula[0][2]!=" ":
            return True
        
        return False
    
    def verificar_jugada(self, fila, columna):
        #Verificar si la casilla esta vacia
        if self._cuadricula[fila][columna]!=" ":
            return False
        return True
    
    def colocar_ficha(self, fila, columna, ficha):
        #Colocar una ficha en una casilla
        self._cuadricula[fila][columna]=ficha
        
    def mostrar_tablero(self):
        
        #Mostrar el tablero en pantalla con los simbolos de las fichas en tamaño grande
        myString="""
        {} | {} | {}
        ---------
        {} | {} | {}
        ---------
        {} | {} | {}
        """        
        myString=myString.format(self._cuadricula[0][0], self._cuadricula[0][1], self._cuadricula[0][2], self._cuadricula[1][0], self._cuadricula[1][1], self._cuadricula[1][2], self._cuadricula[2][0], self._cuadricula[2][1], self._cuadricula[2][2])
        print(myString)
        

            

            

                
            

                
        
        
        
        
        


        
        
        
        
        
        
        